## TypeScript-Definitions-for-Wechat

❗️ It's Simplified, just for intelliSense and practice.

## API Contents
1. 网络  ✔️
2. 媒体  ✔️
3. 文件
4. 数据缓存
5. 位置
6. 设备
7. 界面
8. WXML节点信息
9. 第三方平台
10. 开放接口
11. 调试接口